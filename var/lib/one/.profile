# ~/.profile: executed by Bourne-compatible login shells.

if [ "$BASH" ]; then
  if [ -f ~/.bashrc ]; then
    . ~/.bashrc
  fi
fi

mesg n

alias create-vm='onetemplate instantiate "165-Ubuntu(EYWA)" --name'
alias create-vr='onetemplate instantiate "165-ONE-Router" --name'
alias delete-vr='onevm delete'

